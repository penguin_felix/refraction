int screenW = 1000;
int screenH = 600;

void setup() {
  color1 = color(255);
  color2 = color(0);
  
  noSmooth();
  //noLoop();
  setGradient(0, 0, width, height, color1, color2, yAxis);
  
  frameRate(120);
  stroke(0,255,255);
}

void settings() {
  size(screenW,screenH);  // 18:9 aspect ratio
  // logic:
  // background color at point will decide the density
}

void draw() {
  drawLine();
}

void keyPressed() {
  // the key that was pressed is variable 'key'
}
