void drawLine() {
  lineAngleRadians = lineAngle * PI / 180;

  //line(lineEnd[0], lineEnd[1], 
  double[] temp = calculateEndPoint();
  float[] newEnd = new float[] {(float)(temp[0]), (float)(temp[1])};

  line(lineEnd[0], lineEnd[1], newEnd[0], newEnd[1]);
  println(newEnd[0] + " " + newEnd[1]);
  
  lineEnd = newEnd;
}

double[] calculateEndPoint() {
  float length = 10;

  double endX   = lineEnd[0] + length * Math.sin(lineAngleRadians);
  double endY   = lineEnd[1] + length * Math.cos(lineAngleRadians);

  return new double[] {endX, endY};
}

float calculateDiagonal(float w, float h) {
  float cSquared = (pow(w, 2)) + (pow(h, 2));
  return pow(cSquared, 0.5);
}

void nextAngle() {
}

int lineLength = 0;
float[] lineEnd = {screenW/2, screenH/2};
float lineAngle = 45;  // angle from TL to BR is ~63.45
float lineAngleRadians = lineAngle * PI / 180;
